#!../../bin/linux-x86_64/generic_motor

< envPaths

dbLoadDatabase("$(TOP)/dbd/generic_motor.dbd")
generic_motor_registerRecordDeviceDriver(pdbbase)

set_requestfile_path(".", "")
set_requestfile_path("$(MOTOR)/db/", "")
set_requestfile_path("$(TOP)/db/", "")

set_savefile_path("/opt/epics/autosave")
set_pass0_restoreFile("settings.sav","P=$(IOC_PREFIX)")
set_pass1_restoreFile("settings.sav","P=$(IOC_PREFIX)")
save_restoreSet_DatedBackupFiles(0)

#Load the motor.substitutions file specific to motor controller
dbLoadTemplate("motor.substitutions", "P=$(IOC_PREFIX):")

#run motor controller specific startup code
<motor.cmd

iocInit

create_monitor_set("settings.req",5, "P=$(IOC_PREFIX)")
create_monitor_set("aux_settings.req",5, "P=$(IOC_PREFIX)")


## motorUtil (allstop & alldone)
motorUtilInit("P=$(IOC_PREFIX):")
# Boot complete
date

fdbrestore("aux_settings.sav")

